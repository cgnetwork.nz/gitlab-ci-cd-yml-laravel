Required variables:
*  `GITLAB_PRIVATE_TOKEN`
*  `SSH_PRIVATE_KEY`
*  `REMOTE_SERVER_IP`
*  `REMOTE_SERVER_USERNAME`
*  `GIT_USERNAME`
*  `GIT_EMAIL`

Include in your project .gitlab-ci.yml <br>
`include: remote: 'https://gitlab.com/cgnetwork.nz/gitlab-ci-cd-yml-laravel/raw/master/.gitlab-ci.yml'`